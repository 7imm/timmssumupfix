# Timm's SUMup Fix

A fix for the [SUMup snow density subdataset](https://arcticdata.io/catalog/view/doi:10.18739/A26D5PB2S).

For all people out there interested in firn a great compilation of firn density measurements exists. It was assembled by [Lora Koenig](https://nsidc.org/research/bios/koenig.html) and [Lynn Montgomery](https://www.colorado.edu/lab/icesheetclimate/lynn-nicki-montgomery) and is available at the [NSF Arctic Data Center](https://arcticdata.io/catalog/view/doi:10.18739/A26D5PB2S).

> Lora Koenig and Lynn Montgomery. 2019. *Surface Mass Balance and Snow Depth on Sea Ice Working Group (SUMup) snow density subdataset, Greenland and Antartica, 1950-2018.* Arctic Data Center. doi:10.18739/A26D5PB2S.

As I'm interested in modeling the densification of firn, I think this is really great work. But there is a little problem regarding how the dataset is stored. This project aims to fix this problem.

Also check out the [SumUpTools](https://github.com/maximusjstevens/SumUpTools) by [Max Stevens](https://www.ess.washington.edu/people/profile.php?pid=stevens--max), who did a similar thing.


## The Problem

The SUMup snow density subdataset holds abouth three thousand individual firn profiles and single point density measurements. They are stored in a [netCDF](https://www.unidata.ucar.edu/software/netcdf/) file. netCDF files are basically a way to save big tables in a efficent way. The SUMup snow density subdataset looks somewhat like this.

```
  date   |   lat   |   lon   |       |    z    | density
---------|---------|---------|  ...  |---------|---------
   [0]   |   [0]   |   [0]   |       |   [0]   |   [0]
   [0]   |   [0]   |   [0]   |       |   [0]   |   [0]
   [0]   |   [0]   |   [0]   |       |   [0]   |   [0]
   [1]   |   [1]   |   [1]   |       |   [1]   |   [1]
   [2]   |   [2]   |   [2]   |       |   [2]   |   [2]
   [2]   |   [2]   |   [2]   |       |   [2]   |   [2]

        ...       ...       ...     ...       ...

  [n-2]  |  [n-2]  |  [n-2]  |       |  [n-2]  |  [n-2]
  [n-2]  |  [n-2]  |  [n-2]  |       |  [n-2]  |  [n-2]
  [n-1]  |  [n-1]  |  [n-1]  |       |  [n-1]  |  [n-1]
  [n-1]  |  [n-1]  |  [n-1]  |       |  [n-1]  |  [n-1]
   [n]   |   [n]   |   [n]   |       |   [n]   |   [n]
   [n]   |   [n]   |   [n]   |       |   [n]   |   [n]
   [n]   |   [n]   |   [n]   |       |   [n]   |   [n]
---------|---------|---------|  ...  |---------|---------
```

The columns in the table are defining the different properties stored in the file. These properties are:

  * Date
  * Latitude
  * Longitude
  * Density
  * Start_Depth
  * Stop_Depth
  * Midpoint
  * Error
  * Elevation
  * SDOS_Flag
  * Method
  * Citation

To get information about all properties see the [documentation](https://cn.dataone.org/cn/v2/resolve/urn:uuid:7785ea08-6394-4ae9-9a8d-bdb45d958e7f) of the SUMup snow density subdataset. Nevertheless one can divide the fields in *metdata*, like the longitude, and *datapoints*, like for example the density. One row in the table is assigned to one datapoint. Individual profiles are saved one after another.

Saving the data this way results in a lot of redundance as the metadata are stored for every datapoint. For example if one firn profile consists of thousand datapoints, the metadata are saved a thousand times, too. Even if they don't change.

But okay... not really a drawback. The problem I really have troubled with are the sequentially saved profiles and single point measurements, as the dataset lacks of an identifier for them, like a numbering. In fact the data table looks more like this.

```
  date   |   lat   |   lon   |       |    z    | density
---------|---------|---------|  ...  |---------|---------
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]

        ...       ...       ...     ...       ...

   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
   [?]   |   [?]   |   [?]   |       |   [?]   |   [?]
---------|---------|---------|  ...  |---------|---------
```

So one have to find the boundaries where one profile ends and another one starts oneself. This projects aims to find and provide these boundaries and to reorganize the data in the way indicated below.

Every row of the datatable shall represent one measurement (firn profile or single measurement). In this way the metadata for one measurement only has to be saved once. The datapoint fields are holding arrays of variable length storing the remaining data. In this way the file size of the dataset can be reduced by about 60 % and individual measurements are easily accessible.

```
  date   |   lat   |   lon   |       |         z         |     density
---------|---------|---------|  ...  |-------------------|---------------------
   [0]   |   [0]   |   [0]   |       | [0], [0], [0]     | [0], [0], [0]
   [1]   |   [1]   |   [1]   |       | [1]               | [1]
   [2]   |   [2]   |   [2]   |       | [2], [2], [2], ...| [2], [2], [2], ...

        ...       ...       ...     ...                 ...

  [n-2]  |  [n-2]  |  [n-2]  |       | ..., [n-2], [n-2] | ..., [n-2], [n-2]
  [n-1]  |  [n-1]  |  [n-1]  |       | [n-1], [n-1]      | [n-1], [n-1]
   [n]   |   [n]   |   [n]   |       | [n], [n], [n]     | [n], [n], [n]
---------|---------|---------|  ...  |-------------------|---------------------
```


## How it works

Below the methods used to identifiy individual profiles and to store the data are described briefly. These methods are **not perfect**. There are still some problems left. For more information see the *known issues* section below.

### NaN values

The tool identifies NaN values in "critical data fields" as I call it and deletes such rows from the data table. The "critical data fields" are *Density*, *Midpoint*, *Latitude* and *Longitude*. In my opinion if one of these information is missing the datapoint is worthless and can be deleted.

### Profile identification

For the identification of individual measurements in the dataset two methods are combined.

  1. evaluation of the "Midpoint-gradient"
  2. hashing the metadata

Both methods when used alone have problems identifying all measurements. And I know even with both methods there are still some problems in the data. **So be careful out there, if you are using the data and this tool!**

#### "Midpoint-gradient"

The filed *Midpoint* in the dataset holds the depth information of a datapoint. See the [documentation](https://cn.dataone.org/cn/v2/resolve/urn:uuid:7785ea08-6394-4ae9-9a8d-bdb45d958e7f) of the dataset to uderstand the distinction between *Midpoint*, *Start_Depth* and *Stop_Depth* (basically this originates from the method used for the measurement). The mehthod is based on the assumption that all measurements are stored sequentially in the data table and that all measurements are sorted in the way that the depth is increasing. If this is true the "gradient" of the depth calculatetd in this fashion

```
dz = z[n+1] - z[n]
```

becomes negative if a new profile begins.

This only works if the last datapoint of a profile lies below the first datapoint of the next profile. This is especially a problem when it comes to single point measurements. So another method in combination is needed.

#### Hashing the metadata

As outlined before the metadata are stored for every datapoint. So datapoints from one profile should share common metadata. To test this the fields *Date*, *Latitude*, *Longitude*, *Method*, *Citation* and *SDOS_Flag* are combined for every row in the datatable and a hash using python3's `hash()` function is calculated over the metadata.

The problem with this method is, that multiple measurements can share the same metadata.

### Storing the new aranged data

The modified data are stored in a new netCDF4 file. There exists a data structure in netCDF4 called *VLType*. This type allows to create *variable-length* or *ragged* arrays.


## Requirements

I tried to limit the requirements as much as possible. The only things really required are [python3](https://www.python.org/) and the [python interface for netCDF](https://github.com/Unidata/netcdf4-python). Latter can for example easily be installed using [pip](https://github.com/pypa/pip).

```bash
pip3 install netCDF4
```

I wanted to solve the problem entirely using python3 not relying on [numpy](https://numpy.org/). But I wasn't very pleased with the speed the profiles are identified. So I included a version relying on numpy. So if one wants to identify the profiles again numpy is used, if it is available. If not it's done using pure python3, which also works but is a little slower. Nevertheless the fix can also be used not identifying the profiles but using stored boundaries (see below).


## How to use the fix

Besides *README*, *LICENSE* and *.gitignore* there are two files in the repository. *fix.py* and *fix_info.dat*. *fix.py* is the python script that actually does things. *fix_info.dat* stores the information where NaN values exist in "critical fields" and where a profile or single point measurement begins and ends. To apply the fix to the SUMup snow density subdataset (even if does not exist on your machine) simply run *fix.py*.

```bash
./fix.py
```

Whats going to happen:

  1. The script will check if a file with the name *sumup_density_2019.nc* is present in the current working directory. This file is the original netCDF file of the SUMup snow density subdataset. If the does not exist, the script will download it from the *NSF Arctic Data Center*.
  2. Next the script will check if the file *fix_info.dat* is present. As described before this file holds the information on how to fix the dataset. As in theory this information has to be gathered only once, I thought one could save some electricity adding this information to the repository. So if the file exists the script will read the information, apply it to the dataset and write a new netCDF file.
  3. Let's assume the file isn't present (maybe it was deleted by accident). Then the script will search the data for NaN values in "critical fields" as described above.
  4. After finding NaN values the *fix.py* will identify the individual profiles.
  5. Having gathered all information, these information will be written into a new file *fix_info.dat*.
  6. Last, the fix will be applied to the dataset and a new netCDF file with the name *timms_density_2019.nc* will be written to the current working directory.


## Known issues

The SUMup fix only sorts out some NaN values from the original dataset, tries to identify individual measurements and reorganizes the way the data is stored. Nevertheless there are some known issues regarding the original and the modified dataset that don't get addressed in this (version of) the fix.

  * Some of the density data are saved using the unit g/cm^3, some use kg/m^3 (the right unit in my opinion).
  * Theres one profile hitting the Bullseye, geographic south pole. This coordinate is wrong (also see the [SumUpTools](https://github.com/maximusjstevens/SumUpTools)).
  * After applying the fix and plotting the data two profiles can be recognized which are obviously including a wrong datapoint at their base.


## License
This project is [beerware licensed](https://en.wikipedia.org/wiki/Beerware).

The [SUMup snow density subdataset](https://arcticdata.io/catalog/view/doi:10.18739/A26D5PB2S) is published under the [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).